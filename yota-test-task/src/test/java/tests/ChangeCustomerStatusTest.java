package tests;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;
import service.BaseTest;
import steps.ApiAuthSteps;

public class ChangeCustomerStatusTest extends BaseTest {
    ApiAuthSteps apiAuthSteps = new ApiAuthSteps();
    String status = "EDIT";

    @Epic(value = "Тестовое задание Yota")
    @Feature(value = "Бизнес-сценарий «Активация абонента»")
    @Test(description = "ТК№78 Ручная активация неактивированного кастомера", groups = "active")
    public void changeCustomerStatus(@Optional(value = "ADMIN") String role) {
        String id = apiAuthSteps.createCustomer(admin.getToken(),
                apiAuthSteps.getListPhoneAndPhones(admin.getToken()));
        apiAuthSteps.changeCustomerStatus(admin.getToken(), id, status);
        apiAuthSteps.checkStatusCustomerById(admin.getToken(), id, status);
    }
}
