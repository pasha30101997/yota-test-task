package tests;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import service.BaseTest;
import service.Employer;
import steps.ApiAuthSteps;

public class SaveNewCustomerOldSystemTest extends BaseTest {
    ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    @Epic(value = "Тестовое задание Yota")
    @Feature(value = "Бизнес-сценарий «Активация абонента»")
    @Test(description = "ТК№59,68 Получение кастомера по действующему номеру телефона",
            dataProvider = "token", groups = "active")
    public void findCustomerByPhoneNumber(Employer employer){
        apiAuthSteps.findByPhoneNumber(employer.getToken(),
                apiAuthSteps.createCustomer(employer.getToken(),
                        apiAuthSteps.getListPhoneAndPhones(employer.getToken())));
    }
}
