package tests;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import service.BaseTest;
import service.Employer;
import steps.ApiAuthSteps;


public class ListFreeNumbersTest extends BaseTest {
    ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    @Epic(value = "Тестовое задание Yota")
    @Feature(value = "Бизнес-сценарий «Активация абонента»")
    @Test(description = "ТК№7-8 Получение списка свободных номеров номеров",
            dataProvider = "token", groups = "active")
    public void getListPhones(Employer employer) {
        apiAuthSteps.checkListPhoneForNull(employer.getToken());
    }
}
