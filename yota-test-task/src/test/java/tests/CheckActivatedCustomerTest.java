package tests;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import service.BaseTest;
import service.Employer;
import steps.ApiAuthSteps;

public class CheckActivatedCustomerTest extends BaseTest {
    ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    @Epic(value = "Тестовое задание Yota")
    @Feature(value = "Бизнес-сценарий «Активация абонента»")
    @Test(description = "ТК№50,54 Получение кастомера по действительному id",
            dataProvider = "token", groups = "active")
    public void checkActivatedCustomer(Employer employer) {
        apiAuthSteps.checkStatusCustomerById(
                employer.getToken(),
                apiAuthSteps.createCustomer(employer.getToken(),
                        apiAuthSteps.getListPhoneAndPhones(employer.getToken())),
                "ACTIVE");
    }
}