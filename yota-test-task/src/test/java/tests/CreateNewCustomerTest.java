package tests;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
import service.BaseTest;
import service.Employer;
import steps.ApiAuthSteps;

public class CreateNewCustomerTest extends BaseTest {
    ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    @Epic(value = "Тестовое задание Yota")
    @Feature(value = "Бизнес-сценарий «Активация абонента»")
    @Test(description = "ТК№11,30 Создание кастомера",
            dataProvider = "token",  groups = "active")
    public void createNewCustomer(Employer employer) {
        apiAuthSteps.createCustomer(employer.getToken(),
                apiAuthSteps.getListPhoneAndPhones(employer.getToken()));
    }
}
