package service;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.ResponseSpecification;
import lombok.experimental.Delegate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static io.restassured.config.XmlConfig.xmlConfig;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.xml.HasXPath.hasXPath;

public class SoapClient {
    private final String endpoint;
    private final ClassPathTemplateLoader classPathTemplateLoader;
    private ResponseSpecification responseSpecification;
    private Map<String, String> requestHeaders;

    public SoapClient(String endpoint, String templatePrefix, String templateSuffix) {
        this.endpoint = endpoint;
        this.classPathTemplateLoader = new ClassPathTemplateLoader(templatePrefix, templateSuffix);
    }

    public SoapClient(String endpoint, String templatePrefix) {
        this(endpoint, templatePrefix, ".xml");
    }

    public SoapClient(String endpoint) {
        this(endpoint, "");
    }

    /**
     * Служебная функция для конвертации
     */
    private static Map<String, String> headersToMap(Headers headers) {
        return headers.asList().stream().collect(Collectors.toMap(Header::getName, Header::getValue));
    }

    /**
     * Установить ResponseSpecification для всех запросов этого экземпляра клиента
     */
    public SoapClient setResponseSpecification(ResponseSpecification responseSpecification) {
        this.responseSpecification = responseSpecification;
        return this;
    }

    /**
     * Заменяет существующие заголовки запроса для всех запросов этого экземпляра клиента
     */
    public SoapClient setRequestHeaders(Map<String, String> headers) {
        this.requestHeaders = new HashMap<>(headers);
        return this;
    }

    /**
     * Заменяет существующие заголовки запроса для всех запросов этого экземпляра клиента
     */
    public SoapClient setRequestHeaders(Headers headers) {
        this.requestHeaders = headersToMap(headers);
        return this;
    }

    private SoapRequest setRequestParameters(SoapRequest request) {
        if (responseSpecification != null)
            request.setResponseSpecification(responseSpecification);
        if (requestHeaders != null)
            request.setRequestHeaders(requestHeaders);

        return request;
    }

    public SoapRequest newRequest(String templateName, Object templateParams) {
        return setRequestParameters(
                new SoapRequest(endpoint, classPathTemplateLoader, templateName, templateParams));
    }

    public SoapRequest newRequest(String templateName) {
        return setRequestParameters(
                new SoapRequest(endpoint, classPathTemplateLoader, templateName, null));
    }

    public static class SoapRequest {
        private static final Map<String, String> defaultRequestHeaders =
                new HashMap<String,String>(){{put("Content-Type", "application/xml;charset=utf-8");}};

        private static final ResponseSpecification defaultResponseSpecification = new ResponseSpecBuilder()
                .expectBody(not(hasXPath("/Envelope/Body/Fault")))
                .expectStatusCode(200)
                .build();

        private final String endpoint;
        private final ClassPathTemplateLoader classPathTemplateLoader;

        private final String templateName;
        private Object templateParams;
        private Map<String, String> requestHeaders;
        private ResponseSpecification responseSpecification;

        protected SoapRequest(String endpoint, ClassPathTemplateLoader classPathTemplateLoader, String templateName, Object templateParams) {
            this.endpoint = endpoint;
            this.classPathTemplateLoader = classPathTemplateLoader;
            this.templateName = templateName;
            this.templateParams = templateParams;
            this.requestHeaders = new HashMap<>(defaultRequestHeaders);
            this.responseSpecification = defaultResponseSpecification;
        }

        /**
         * Заменяет существующие заголовки запроса
         */
        public SoapRequest setRequestHeaders(Map<String, String> headers) {
            this.requestHeaders = new HashMap<>(headers);
            return this;
        }

        /**
         * Заменяет существующие заголовки запроса
         */
        public SoapRequest setRequestHeaders(Headers headers) {
            return setRequestHeaders(headersToMap(headers));
        }

        /**
         * Добавляет заголовок запроса
         */
        public SoapRequest addRequestHeader(Header header) {
            this.requestHeaders.put(header.getName(), header.getValue());
            return this;
        }

        public SoapRequest setTemplateParams(Object templateParams) {
            this.templateParams = templateParams;
            return this;
        }

        public SoapRequest setResponseSpecification(ResponseSpecification responseSpecification) {
            this.responseSpecification = responseSpecification;
            return this;
        }

        private String renderTemplate() throws IOException {
            return new Handlebars(classPathTemplateLoader).compile(templateName).apply(templateParams);
        }

        public SoapResponse call() throws IOException {
            /* @formatter:off */
            return new SoapResponse(
                    given()
                            .config(RestAssured.config().xmlConfig(xmlConfig().with().namespaceAware(false)))
                            .filter(new AllureRestAssured())
                            .headers(requestHeaders)
                            .basePath(endpoint)
                            .body(renderTemplate())
                    .when()
                            .log().all()
                            .post(endpoint)
                    .then()
                            .log().all()
                            .spec(responseSpecification)
            );
            /* @formatter:on */
        }

    }

    public static class SoapResponse {
        @Delegate
        private final ValidatableResponse validatableResponse;
        @Delegate
        private final XmlPath xmlPath;

        public SoapResponse(ValidatableResponse response) {
            this.validatableResponse = response;
            xmlPath = new XmlPath(validatableResponse.extract().asString());
        }

        @Override
        public String toString() {
            return validatableResponse.extract().asString();
        }

    }

}
