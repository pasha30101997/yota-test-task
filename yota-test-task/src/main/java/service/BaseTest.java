package service;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import steps.ApiAuthSteps;

public class BaseTest {
    public static Employer admin;
    public static Employer user;

    ApiAuthSteps apiAuthSteps = new ApiAuthSteps();

    @BeforeSuite(description = "Пользователь авторизуется в системе", alwaysRun = true)
    public void InitTest() {
        String token = apiAuthSteps.
                getAuthToken(System.getProperty("loginAdmin"),
                        System.getProperty("passwordAdmin"));
        admin = new Employer(token, Employer.roleEnum.ADMIN);
        token = apiAuthSteps.
                getAuthToken(System.getProperty("loginUser"),
                        System.getProperty("passwordUser"));
        user = new Employer(token, Employer.roleEnum.USER);
    }

    @DataProvider(name = "token", parallel = true)
    public Object[][] dataProviderToken() {
        return new Object[][]{
                {admin}, {user}
        };
    }
}
