package service.api;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.given;

public class RequestSpecificationApi {

    public static final RequestSpecification REQUEST_SPECIFICATION_JSON =
            new RequestSpecBuilder()
                    .setBaseUri(System.getProperty("baseUrl"))
                    .addFilter(new AllureRestAssured())
                    .setContentType(ContentType.JSON)
                    .build();

    public static final ResponseSpecification RESPONSE_SPECIFICATION =
            new ResponseSpecBuilder()
                    .expectStatusCode(200)
                    .build();

}
