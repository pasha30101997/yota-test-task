package service.pojo.response;

import lombok.Data;

@Data
public class PhoneResponsePOJO {
	private Long phone;
	private String locale;
}
