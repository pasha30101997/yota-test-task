package steps;

import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.testng.Assert;
import service.pojo.request.AuthRequestPOJO;
import service.pojo.request.ChangeCustomerStatusRequestPOJO;
import service.pojo.request.createCustomer.AdditionalParameters;
import service.pojo.request.createCustomer.CreateCustomerRequestPOJO;
import service.pojo.response.PhoneResponsePOJO;
import service.SoapClient;
import service.api.RequestSpecificationApi;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;

public class ApiAuthSteps {
    public Long successPhone;
    AuthRequestPOJO authRequestPOJO = new AuthRequestPOJO();
    CreateCustomerRequestPOJO createCustomerRequestPOJO = new CreateCustomerRequestPOJO();
    ChangeCustomerStatusRequestPOJO changeCustomerStatusRequestPOJO = new ChangeCustomerStatusRequestPOJO();
    static SoapClient routingService_ApiUrl = new SoapClient(System.getProperty("baseUrl") + "/customer/findByPhoneNumber", "/request");

    @Step("Получение authToken")
    public String getAuthToken(String login, String password) {
        return given()
                .spec(RequestSpecificationApi.REQUEST_SPECIFICATION_JSON)
                .body(authRequestPOJO.withLogin(login).withPassword(password))
                .when()
                .post("/login")
                .then()
                .log().all()
                .spec(RequestSpecificationApi.RESPONSE_SPECIFICATION)
                .extract().path("token");
    }

    @Step("Получение списка свободных номеров")
    private Response getEmptyPhone(String token) {
        return given()
                .spec(RequestSpecificationApi.REQUEST_SPECIFICATION_JSON)
                .headers("authToken", token)
                .when()
                .get("/simcards/getEmptyPhone")
                .then()
                .log().all()
                .extract().response();

    }

    @Deprecated
    @Step("Получение списка свободных номеров и первого номера из массива")
    public Long getListPhoneAndPhone(String token) {
        AtomicReference<Long> phone = new AtomicReference<>();
        await()
                .pollInterval(Duration.ofMillis(500))
                .atMost(Duration.ofMinutes(1))
                .until(() -> {
                    Response response = getEmptyPhone(token);
                    boolean success = response.statusCode() == 200 && response.jsonPath().getList("phones").size() > 0;
                    if (success) phone.set(response.path("phones[0].phone"));
                    return success;
                });
        return phone.get();
    }

    @Step("Получение спика номеров и проверка на пустой список или на 500 статус код")
    public List<String> getListPhoneAndPhones(String token) {
        AtomicReference<List<String>> phone = new AtomicReference<>();
        await()
                .pollInterval(Duration.ofMillis(500))
                .atMost(Duration.ofMinutes(1))
                .until(() -> {
                    Response response = getEmptyPhone(token);
                    boolean success = response.statusCode() == 200 && response.jsonPath().getList("phones").size() > 0;
                    if (success)
                        phone.set(response.body().jsonPath().getList("phones", PhoneResponsePOJO.class)
                                .stream().map(p -> Long.toString(p.getPhone())).collect(Collectors.toList()));
                    return success;
                });
        return phone.get();
    }

    @Step("Проверка на пустой список или на 500 статус код ")
    public void checkListPhoneForNull(String token) {
        await()
                .pollInterval(Duration.ofMillis(500))
                .atMost(Duration.ofMinutes(1))
                .until(() -> {
                    Response response = getEmptyPhone(token);
                    return response.statusCode() == 200 && response.jsonPath().getList("phones").size() > 0;
                });
    }

    @Step("Создание нового кастомера")
    public String createCustomer(String token, List<String> phone) {
        for (int i = 0; i <= phone.size() - 1; i++) {
            Response response = given()
                    .spec(RequestSpecificationApi.REQUEST_SPECIFICATION_JSON)
                    .headers("authToken", token)
                    .body(createCustomerRequestPOJO
                            .withName("AutoTest")
                            .withPhone(phone.get(i))
                            .withAdditionalParameters(new AdditionalParameters().withString("string")))
                    .when()
                    .post("/customer/postCustomer")
                    .then()
                    .log().all()
                    .extract().response();
            if (response.statusCode() == 200) {
                successPhone = Long.valueOf(phone.get(i));
                return response.path("id");
            }
        }
        Assert.fail("Все номера были ранее использованы");
        return null;
    }

    @Step("Получение сведений Customer по ID = {id}")
    private JsonPath getCustomerById(String token, String id) {
        return given()
                .spec(RequestSpecificationApi.REQUEST_SPECIFICATION_JSON)
                .header("authToken", token)
                .param("customerId", id)
                .when()
                .get("/customer/getCustomerById")
                .then()
                .log().all()
                .extract().jsonPath();
    }

    @Step("Проверка смены текущего статуса на {status} у созданного Customer в течении 2 минут")
    public void checkStatusCustomerById(String token, String id, String status) {
        await()
                .pollInterval(Duration.ofSeconds(1))
                .atMost(Duration.ofMinutes(2))
                .until(() -> {
                    JsonPath response = getCustomerById(token, id);
                    assertThat(response.get("return.additionalParameters.string"),Matchers.equalTo("string"));
                    assertThat(response.get("return.pd").toString().
                            replaceAll("[^0-9,]","").split(",")[0].length(),
                            Matchers.equalTo(6));
                    assertThat(response.get("return.pd").toString().
                                    replaceAll("[^0-9,]","").split(",")[1].length(),
                            Matchers.equalTo(4));
                    return response.get("return.status").equals(status);
                });
    }

    @SneakyThrows
    @Step("Проверка сохранения Customer в старой системе {id}")
    public void findByPhoneNumber(String token, String id) {
        String oldId = routingService_ApiUrl.newRequest("findByPhoneNumber", new HashMap<String, String>() {{
                    put("token", token);
                    put("phone", successPhone.toString());
                }})
                .call().getString("Envelope.Body.customerId");
        assertThat(oldId, Matchers.equalTo(id));
    }

    @Step("Изменение статуса Customer на status = {newStatus} c id = {id} ")
    public void changeCustomerStatus(String token, String id, String status){
        given()
                .spec(RequestSpecificationApi.REQUEST_SPECIFICATION_JSON)
                .header("authToken", token)
                .body(changeCustomerStatusRequestPOJO.withStatus(status))
                .when()
                .post("/customer/"+ id +"/changeCustomerStatus")
                .then()
                .log().all()
                .spec(RequestSpecificationApi.RESPONSE_SPECIFICATION);
    }
}
