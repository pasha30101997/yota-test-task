# yota-test-task



## Окружение
1. Java 8
2. Maven 3.6.3
3. Intellij IDEA
4. Docker
5. Docker-compose

## Подготовка тестовой среды
### Запуск тестовой среды в Docker

1. Сделать клонирование проекта на локальную машину
```
git clone https://gitlab.com/pasha30101997/yota-test-task.git
```
2. Открыть консоль в директории yota-test-task
3. Выполнить команду
```
docker-compose up -d
```

### Запуск тестовой среды на локальной машине

1. Сделать клонирование проекта на локальную машину
```
git clone https://gitlab.com/pasha30101997/yota-test-task.git
```
2. Открыть консоль в директории test-app
3. Выполнить команду 
```
java -jar /usr/local/lib/testService-1.0-SNAPSHOT.jar
```

## Запуск тестов через IDEA
1. Сделать клонирование проекта на локальную машину
```
git clone https://gitlab.com/pasha30101997/yota-test-task.git
```
2. Открыть проект yota-test-task/yota-test-task в IDEA
3. Создать конфигурацию запуска. Указать в полях:
   1. Test kind "Group"
   2. Group "active"
   3. VM options 
```
-DloginAdmin=admin -DpasswordAdmin=password -DloginUser=user -DpasswordUser=password -DbaseUrl=http://localhost:8090
```
4. Запустить конфигурацию
5. После прогона тестов справа во вкладке Maven в директории Plugins открыть allure и выполнить allure:serve для просмотра отчетов

## Альтернативный запуск
1. Сделать клонирование проекта на локальную машину
```
git clone https://gitlab.com/pasha30101997/yota-test-task.git
```
2. Открыть проект yota-test-task/yota-test-task в IDEA
3. Запустить консоль и выполнить команду 
```
mvn -DloginAdmin=admin -DpasswordAdmin=password -DloginUser=user -DpasswordUser=password -DbaseUrl=http://localhost:8090 -Dgroup=active test
```
4. После прогона тестов для отображения отчета выполнить команду
```
mvn allure:serve
```
